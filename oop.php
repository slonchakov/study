<?php

/*
 * Абстракция данных
Абстрагирование означает выделение значимой информации и исключение из рассмотрения незначимой.
В ООП рассматривают лишь абстракцию данных (нередко называя её просто «абстракцией»),
подразумевая набор наиболее значимых характеристик объекта, доступных остальной программе.


Encapsulation
Encapsulation is an object-oriented programming concept that binds together the data and functions that manipulate the data,
and that keeps both safe from outside interference and misuse.
Data encapsulation led to the important OOP concept of data hiding.
If a class does not allow calling code to access internal object data and permits access through methods only,
this is a strong form of abstraction or information hiding known as encapsulation.
Some languages (Java, for example) let classes enforce access restrictions explicitly,
for example denoting internal data with the private keyword and designating methods intended for use by code outside the class with the public keyword.
Methods may also be designed public, private, or intermediate levels such as protected (which allows access from the same class and its subclasses,
but not objects of a different class).
In other languages (like Python) this is enforced only by convention (for example, private methods may have names that start with an underscore).
Encapsulation prevents external code from being concerned with the internal workings of an object.
This facilitates code refactoring, for example allowing the author of the class to change how objects of that class represent their data internally
without changing any external code (as long as "public" method calls work the same way).
It also encourages programmers to put all the code that is concerned with a certain set of data in the same class,
which organizes it for easy comprehension by other programmers.
Encapsulation is a technique that encourages decoupling.
--translation
Инкапсуляция - это объектно-ориентированная концепция программирования, которая связывает воедино данные и методы, которые манипулируют данными.
и это защищает как от внешнего вмешательства, так и от неправильного использования.
Инкапсуляция данных привела к важной ООП-концепции сокрытия данных.
Если класс не позволяет вызывающему коду обращаться к внутренним данным объекта и разрешает доступ только через методы,
это сильная форма абстракции или сокрытия информации, известная как инкапсуляция.
Некоторые языки (например, Java) позволяют классам явно применять ограничения доступа,
например, обозначение внутренних данных с помощью ключевого слова private и назначение методов, предназначенных для использования кодом вне класса с ключевым словом public.
Методы также могут быть спроектированы как открытый, частный или промежуточный уровни, например protected (который обеспечивает доступ из того же класса и его подклассов,
но не объекты другого класса).
В других языках (например, Python) это обеспечивается только соглашением (например, частные методы могут иметь имена, начинающиеся с подчеркивания).
Инкапсуляция предотвращает взаимодействие внешнего кода с внутренней работой объекта.
Это облегчает рефакторинг кода, например, позволяет автору класса изменять, как объекты этого класса представляют свои данные внутренне
без изменения какого-либо внешнего кода (при условии, что «публичные» вызовы методов работают одинаково).
Это также побуждает программистов размещать весь код, связанный с определенным набором данных, в одном классе,
который организует это для легкого понимания другими программистами.
Инкапсуляция - это техника, которая поощряет разделение.
--//translation

Inheritance
Languages that support classes almost always support inheritance.
This allows classes to be arranged in a hierarchy that represents "is-a-type-of" relationships.
For example, class Employee might inherit from class Person.
All the data and methods available to the parent class also appear in the child class with the same names.
For example, class Person might define variables "first_name" and "last_name" with method "make_full_name()".
These will also be available in class Employee, which might add the variables "position" and "salary".
This technique allows easy re-use of the same procedures and data definitions, in addition to potentially mirroring real-world relationships in an intuitive way.
Rather than utilizing database tables and programming subroutines, the developer utilizes objects the user may be more familiar with: objects from their application domain.

Наследование
Языки, которые поддерживают классы, почти всегда поддерживают наследование.
Это позволяет классам быть организованными в иерархию, которая представляет отношения типа «тип».
Например, класс Employee может наследоваться от класса Person.
Все данные и методы, доступные родительскому классу, также появляются в дочернем классе с такими же именами.
Например, класс Person может определять переменные «first_name» и «last_name» с помощью метода «make_full_name ()».
Они также будут доступны в классе Employee, в который можно добавить переменные «position» и «salary».
Этот метод позволяет легко повторно использовать одни и те же процедуры и определения данных,
в дополнение к потенциальному отражению реальных отношений интуитивно понятным способом.
Вместо использования таблиц базы данных и программных подпрограмм разработчик использует объекты,
с которыми пользователь может быть более знаком: объекты из своей прикладной области.

--------------------------------------------------------------------

Полиморфизм

Параметрический полиморфизм - метод может принимать параметры различных типов
Полиморфизм подтипов - с помощью наследования можем поменять поведение метода в дочернем классе, но есть ограничение
метод должен соответствовать полность родительскому, входные и выходные параметры должны совпадать, иначе будет нарушен принцип Лисков.
принцип Лисков - наследники могут заменить базовый класс без последствий и дополнительных действий со стороны вызывающего кода и
родительского класса и соответствовать базовому интерфейсу.

Polymorphism
Subtyping – a form of polymorphism – is when calling code can be agnostic as to which class in the supported hierarchy
it is operating on – the parent class or one of its descendants.
Meanwhile, the same operation name among objects in an inheritance hierarchy may behave differently.
For example, objects of type Circle and Square are derived from a common class called Shape.
The Draw function for each type of Shape implements what is necessary to draw itself while calling code can remain indifferent to the particular type of Shape being drawn.
This is another type of abstraction which simplifies code external to the class hierarchy and enables strong separation of concerns.

Полиморфизм подтипов - форма полиморфизма - это когда вызывающий код может быть независим от того, с каким классом в поддерживаемой иерархии
он работает с родительским классом или одним из его потомков.
Между тем, одно и то же имя операции среди объектов в иерархии наследования может вести себя по-разному.
Например, объекты типа Circle и Square являются производными от общего класса Shape.
Функция Draw для каждого типа Shape реализует то, что необходимо для рисования, в то время как вызывающий код может оставаться безразличным к конкретному типу рисуемого Shape.
Это еще один тип абстракции, который упрощает код, внешний по отношению к иерархии классов, и обеспечивает сильное разделение задач.
 */

/*
 * Объектно-ориентированное программирование это стиль программирования основынный на использовании объектов, которые являются экзамплярами классов, а классы организуют иерархию наследования или композицию.
 * Объекты объединяют в себе данные и методы манипулирующие этими данными, также позволяют скрыть эти данные от внешнего неправильного или ошибочного воздействия.
 * Уровень защищенности данных и методов контролируется модификаторомами доступа:
 *  - public - элемент объекта доступен во всем коде
 *  - protected - элемент доступен только внутри объекта и в дочерних объектах
 *  - private - элемент досткпен только внутри класса, в котором он был объявлен
 *
 * Классы могут наследовать друие классы расширяя их возможности, создавая иерархии.
 *
 * Также разные классы могут реализовывать один интерфейс делая структуру более гибкой и управляемой
 *
 * Полиморфизм подтипов означает что вызывающий код может быть безразличным, к тому с каким классом из поддерживаемой иерархии он работает.
 * С родительским классом или одним из его потомков. и при этом методы с одним названием могут вести себя по-разному.
 *
 */

/**
 *
 * Интерфейс
 *
 * Интерфейсы объектов позволяют создавать код, который указывает, какие методы должен реализовать класс, без необходимости определять, как именно они должны быть реализованы.
 * Тела методов интерфейсов должны быть пустыми.
 * Для реализации интерфейса используется оператор implements.
 * Интерфейсы могут содержать константы. Константы интерфейсов работают точно так же, как и константы классов, за исключением того, что они не могут быть переопределены наследующим классом или интерфейсом.
 *
 * Абстрактные классы
 *
 * Объекты классов, определенные, как абстрактные, не могут быть созданы, и любой класс, который содержит по крайней мере один абстрактный метод, должен быть определен, как абстрактный.
 * Методы, объявленные абстрактными, несут, по существу, лишь описательный смысл и не могут включать реализацию.
 * При наследовании от абстрактного класса, все методы, помеченные абстрактными в родительском классе, должны быть определены в дочернем классе;
 * кроме того, область видимости этих методов должна совпадать (или быть менее строгой).
 *
 *
 * Трейт
 *
 * это механизм обеспечения повторного использования кода в языках с поддержкой только одиночного наследования, таких как PHP.
 * методы трейта используются классом как собственные методы
 *
 */

/*
 * Паттерны
 * Порождающие шаблоны
 * Структурные шаблоны
 * Поведенческие шаблоны
 */

/*
 * Порождающие шаблоны:
 * Фабричный метод
 * Абстарктная фабрика
 * Статическая фабрика
 * Простая фабрика
 *
 * Строитель
 * Синглтон
 */

/*
 * Factory Method (Фабричный метод)
 * - завистит от абстракций, а не от конкрентых классов
 *
 * Все создваемые фабрики подчиняются одному интерфейсу.
 * Тип обекта возвращаемый фабричным методом привязан к интерфейсу, а не к конкретной реализации
 * Фабричный метод является частью абстарктной фабрики
 *
 * В простейшем случае это класс создающий объект определенного типа и может иметь несколько реализаций
 *
 */

// интерфейс логгера
interface Logger
{
    public function log(string $message);
}

// реализация логгера
class StdoutLogger implements Logger
{
    public function log(string $message)
    {
        echo $message;
    }
}

// реализация логгера
class FileLogger implements Logger
{
    private $filePath;

    public function __construct(string $filePath)
    {
        $this->filePath = $filePath;
    }

    public function log(string $message)
    {
        file_put_contents($this->filePath, $message . PHP_EOL, FILE_APPEND);
    }
}

// интерфес фабрики
interface LoggerFactory
{
    public function createLogger(): Logger;
}

// реализация фабрики для StdoutLogger
class StdoutLoggerFactory implements LoggerFactory
{
    // метод созает конкретный класс, но возвращаемый тип объекта привязан к абстракции
    public function createLogger(): Logger // не StdoutLogger
    {
        return new StdoutLogger();
    }
}

// реализация фабрики для FileLogger
class FileLoggerFactory implements LoggerFactory
{
    private $filePath;

    public function __construct(string $filePath)
    {
        $this->filePath = $filePath;
    }

    // метод созает конкретный класс, но возвращаемый тип объекта привязан к абстракции
    public function createLogger(): Logger
    {
        return new FileLogger($this->filePath);
    }
}

$loggerFactory = new StdoutLoggerFactory();
$logger = $loggerFactory->createLogger();

$loggerFactory = new FileLoggerFactory(sys_get_temp_dir());
$logger = $loggerFactory->createLogger();

/*
 * Abstract Factory (Абстрактная фабрика)
 * Создавать серии связанных или зависимых объектов без указания их конкретных классов. Обычно созданные классы реализуют один и тот же интерфейс.
 * Клиент абстрактной фабрики не заботится о том, как создаются эти объекты, он просто знает, как они работают вместе.
 * Абстрактная фабрика применяется, когда требуется создать семейство интерфейсов, реализация которых должна подменяться совместно
 */

interface WriterFactory
{
    public function createCsvWriter(): CsvWriter;
    public function createJsonWriter(): JsonWriter;
}

interface CsvWriter
{
    public function write(array $line): string;
}

interface JsonWriter
{
    public function write(array $data, bool $formatted): string;
}

/*
 * Реализации фабрики должны уметь создавать два типа объектов CsvWriter и JsonWriter.
 * Реализации этих фабрик это фабричные методы
 */

/*
 * Static Factory (Статическая фабрика)
 * Отличается от абстрактной тем что имеет один статический метод для создания всех разных типов объектов
 * Статическая фабрика плоха тем что:
 *  - имеет глобавльное состояние и не может иметь имитацию (mock)
 *  - не может иметь подклассов и иметь несколько разных реализаций
 */
final class StaticFactory
{
    // метод использует как возвращаемый тип абстракцию
    public static function factory(string $type): Formatter
    {
        // метод возвращает кокретную реализацию интерфейса Formatter
        if ($type == 'number') {
            return new FormatNumber();
        } elseif ($type == 'string') {
            return new FormatString();
        }

        throw new InvalidArgumentException('Unknown format given');
    }
}

interface Formatter
{
    public function format(string $input): string;
}

class FormatString implements Formatter
{
    public function format(string $input): string
    {
        return $input;
    }
}

class FormatNumber implements Formatter
{
    public function format(string $input): string
    {
        return number_format((int) $input);
    }
}

/*
 * Simple Factory (Простая фабрика)
 *
 * Она отличается от Статической Фабрики тем, что собственно не является статической.
 * Таким образом, вы можете иметь множество фабрик с разными параметрами.
 * Простая фабрика всегда должна быть предпочтительнее Статической фабрики!
 */
class SimpleFactory
{
    public function createBicycle(): Bicycle
    {
        return new Bicycle();
    }
}

class Bicycle
{
    public function driveTo(string $destination)
    {
    }
}

$factory = new SimpleFactory();
$bicycle = $factory->createBicycle();
$bicycle->driveTo('Paris');

/*
 * Строитель - для создания сложного объекта, требующего несколько шагов для получения объекта
 */

/*
 * Структурные шаблоны
 *
 * Адаптер
 * Мост
 * Компоновщик
 * Декоратор
 * Фасад
 */

/*
 * Adapter (Адаптер)
 * Привести неудобный или не подходящий интерфейс класса к удобному для конкретного кода
 * если объект имеет методы другого интерфейса мы можем создать класс который обернет эти методы, методами реализующими нужный интерфейс
 */

/*
 * Bridge (Мост)
 *
 * Отделить абстракцию от реализации так, чтобы и то и другое можно было изменять независимо.
 */

/*
 * Поведенческие
 * Стратегия
 */

// Works
// Полиморфизм - если не типизируем возвращаемые данные то можем возвращать что хотим
class A
{
    // public function c(): string <- error on extending
    public function c()
    {
        return '';
    }
}

class B extends A
{
    public function c(): array
    {
        return [];
    }
}

// если не параметризируем и в методе указываем разное количество входных параметров php кидает warning но работает
class D
{
    public function q($a)
    {
        return [$a];
    }
}

class E extends D
{
    public function q($a)
    {
        return $a;
    }
}

var_dump((new E)->q('1'));
var_dump((new D)->q('1'));