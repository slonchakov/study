<?php

namespace Finder;

require 'vendor/autoload.php';
//
//require 'finder/QueriesBuilder.php';
//require 'finder/Finder.php';
//require 'finder/DomParser.php';
//
//spl_autoload();

$queriesBuider = new QueriesBuilder();
$finder = new Finder();
$finder->setQueries($queriesBuider->build());
$finder->process();