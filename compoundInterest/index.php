<?php

$sum = 0;

$term = 't';
$monthly = 'm';
$interest = 'i';
$debug = 'd';

$opts = $term .':'.$monthly.':'. $interest.':'. $debug.'::';

$options = getopt($opts);

if (isset($options[$debug])) {
    var_dump($options);
}

$monthlyInterest = $options[$interest] / 1200;

for ($i=1; $i <= $options[$term]; $i++) {
    $sum += ($sum * $monthlyInterest) + $options[$monthly];
}

print 'For '. $options[$monthly] .' uah per month with interest ' . $options[$interest] ."% yearly \n";
print 'In '. ($options[$term]/12) . ' years you will earn:' ."\n";
print number_format(round($sum, 2), 2, '.', ' ') ." uah \n";
print 'and could get monthly '. round(($sum * $options[$interest] / 100) / 12, 2) ."\n";