<?php

ini_set('display_errors', 1);

require '../vendor/autoload.php';

$conn = new mysqli('localhost', 'root', null,'test');

if ($conn->errno) {
    die('Error of connection'. $conn->errno);
} else {
    echo "Connection is succesful";
}

$title = 'What is Lorem Ipsum?';
$description = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the standard dummy text ever since the 1500s';

$conn->query("TRUNCATE TABLE news");
$conn->query("TRUNCATE TABLE comments");
$start = microtime(true);
for ($i =1; $i <= 10000; $i++) {
    $conn->query("INSERT INTO news (title) VALUES ('". $i . $title."')");

    $newsId = $conn->insert_id;

    $count = rand(0,10);

    for ($j = 1; $j <= $count; $j++) {
        $date = new \Carbon\Carbon();

        if ($count > 5) {
            $date->addDays($i - $j);
            $date->addHours($i - $j);
        } else {
            $date->addDays($i + $j);
            $date->addHours($i + $j);
        }

        $conn->query("INSERT INTO comments (news_id, text, date) VALUES ('$newsId', '". $i . $j . $description."', '". $date->format('Y-m-d H:i:s') ."')");
    }
}
//
//echo $start - microtime(true);
//
// $conn->query("CREATE TABLE news_with_comment_date ( id int PRIMARY KEY AUTO_INCREMENT, title varchar(255), comment_date DATETIME )");
//
//
//$conn->query("TRUNCATE TABLE news_with_comment_date");
//$start = microtime(true);
//$sql = "INSERT INTO news_with_comment_date (title, comment_date) VALUES ";
//// 0.35979795455933
//for ($i =1; $i <= 10000; $i++) {
//
//    $count = rand(0,10);
//    $date = new \Carbon\Carbon();
//    if ($count > 5) {
//        $date->addDays($i );
//        $date->addHours($i);
//    } else {
//        $date->addDays($i);
//        $date->addHours($i);
//    }
//    $sql .= "('". $i . $title."', '". $date->format('Y-m-d H:i:s') ."')";
//    if ($i != 10000) {
//        $sql .= ',';
//    }
//}
//$conn->query($sql);
//echo microtime(true) - $start;


// time of request 11.071815013885
//$start = microtime(true);
//for ($i =1; $i <= 10000; $i++) {
//
//    $count = rand(0,10);
//    $date = new \Carbon\Carbon();
//    if ($count > 5) {
//        $date->addDays($i );
//        $date->addHours($i);
//    } else {
//        $date->addDays($i);
//        $date->addHours($i);
//    }
//    $conn->query("INSERT INTO news_with_comment_date (title, comment_date) VALUES ('". $i . $title."', '". $date->format('Y-m-d H:i:s') ."')");
//}
//echo microtime(true) - $start;