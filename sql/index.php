<?php

ini_set('display_errors', 1);

require '../vendor/autoload.php';

$conn = new mysqli('localhost', 'root', null,'test');

if ($conn->errno) {
    die('Error of connection'. $conn->errno);
} else {
    echo "Connection is succesful";
}

$sql = "select news.*, (select date from comments where comments.news_id = news.id order by date desc limit 1) as comment_date from news order by comment_date desc";

/*
 * Select struct
 */
$sql = "SELECT (DISTINCT) * FROM table [(INNER|LEFT|RIGHT| OUTER)JOIN other_table on(table.id = other_table.foreign_id)] WHERE {condition} GROUP BY {int, col name, formula} [ASC | DESC] HAVING {condition} ORDER BY {col name} [ASC | DESC] LIMIT {offset,}rows";

// Tasks
/*
 * 1. get all news with comments older then 3 days
 */

$sql = "select news.*, 
(select date from comments where comments.news_id = news.id order by date desc limit 1) as comment_date ,
DATE_ADD(NOW(), INTERVAL -3 DAY) as dateadd
from news 
having comment_date < DATE_ADD(NOW(), INTERVAL -3 DAY) 
order by comment_date desc";

/*
 * 2. get more often commented news.
 * 3. group comments by news.
 */

