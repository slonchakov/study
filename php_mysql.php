<?php


/*$connection = new mysqli('127.0.0.1', 'sergeys', 'slon1987', 'study');

$result = $connection->query("SELECT * FROM profiles_emails");
//
//$query->execute();
//$query->bind_result($result);

while ($row = $result->fetch_assoc()) {
    print_r($row);
}*/

/*
try {

    $pdo = new PDO('mysql:host=localhost;dbname=test', 'root', '');

    $request = $pdo->prepare("SELECT * FROM profiles WHERE id > :id");
    $id = 1;

	$id = $pdo->quote($id); // экранирует строку и добавляет кавычки

    $request->execute([
        ':id' => $id
    ]);


    while ($row = $request->fetchObject()) {
        var_dump($row);
    }

} catch (PDOException $e) {
    echo $e->getMessage();
}
*/


/**
 *  Stored procedures Хранимые процедуры
 */

/**
 * Вызов процедуры из PHP
 * $pdo = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
            // execute the stored procedure
            $sql = 'CALL GetCustomers()';
            // call the stored procedure
            $q = $pdo->query($sql);
 */

/**
 * Transaction using PDO
 * wrap instructions in try catch construction
 * if pdo throw PDOException call $pdo->rollBack()


$pdo = new PDO($conStr, self::DB_USER, self::DB_PASSWORD);
 *
try {
	$pdo->beginTransaction();

	$sql = 'SELECT amount FROM accounts WHERE id=:from';
	$stmt = $this->pdo->prepare($sql);
	$stmt->execute(array(":from" => $from));
	$availableAmount = (int) $stmt->fetchColumn();
	$stmt->closeCursor();

	$sql_update_from = 'UPDATE accounts
				SET amount = amount - :amount
				WHERE id = :from';
	$stmt = $this->pdo->prepare($sql_update_from);
	$stmt->execute(array(":from" => $from, ":amount" => $amount));
	$stmt->closeCursor();

	$sql_update_to = 'UPDATE accounts
                                SET amount = amount + :amount
                                WHERE id = :to';
	$stmt = $this->pdo->prepare($sql_update_to);
	$stmt->execute(array(":to" => $to, ":amount" => $amount));
	$pdo->commit(); # the end of transaction, commit all queries

} catch(PDOException $e) {
	$pdo->rollBack();
	die($e->getMessage());
}
 * Transaction Using myhsqli
 * OOP way

$mysqli = new mysqli("127.0.0.1", "my_user", "my_password", "sakila");

if ($mysqli->connect_errno) {
	printf("Connect failed: %s\n", $mysqli->connect_error);
	exit();
}

$mysqli->begin_transaction(MYSQLI_TRANS_START_READ_ONLY);

$mysqli->query("SELECT first_name, last_name FROM actor");
$mysqli->commit();

$mysqli->close();

 * Procedural way

$link = mysqli_connect("127.0.0.1", "my_user", "my_password", "sakila");

if (mysqli_connect_errno()) {
	printf("Connect failed: %s\n", mysqli_connect_error());
	exit();
}

mysqli_begin_transaction($link, MYSQLI_TRANS_START_READ_ONLY);

mysqli_query($link, "SELECT first_name, last_name FROM actor LIMIT 1");
mysqli_commit($link);

mysqli_close($link);
 */
/**
 * Escape user's input
 * mysqli_real_escape_string()
 * mysqli_escape_string — Псевдоним функции mysqli_real_escape_string()
 *
 * $id = mysqli_real_escape_string($connection, $_GET['id]);
 * $id = $mysql->real_escape_string($_GET['id']);
 */