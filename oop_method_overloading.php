<?php

/**
 * overloading перегрузка, вызов метода с использованием магического метода __call
 * - можно вызвать таким образом приватный метод
 * - метод может принимать любое количесто аргументов, таким образом можт быть один метод принимающий разное количество
 *      аргументов
 * - нормальное использование, то как вижу я это написание геттера
 *      getName() => __call($name)
 *      парсить имя метода и получать поле из приватного списка полей
 */
class ABC1 {
    protected function displayMessage() {
        echo 'direct call'. "\n";
    }
}

class BCA extends ABC1
{
    public function __call($method_name1, $arguments) {
        $this->displayMessage();
    }
}

/**
 * это сработает!
 */
/**
 * закоментировал чтоб редактор ошибки не подсвечивал

$obj1 = new BCA;
$obj1->displayMessage();
$obj1->displayMessage(2,3,16,21,12,8);
$obj1->displayMessage(1);
 *
 */