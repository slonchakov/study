<?php

namespace Finder;

class DomParser
{
    private $xpath;
    private $linkPattern = '//a[contains(@href, "toysale")]';

    public function getLinks($body)
    {
        $this->buildXpath($body);
        return $this->xpath->query($this->linkPattern);
    }

    private function buildXpath($body)
    {
        $dom = new \DOMDocument();
        libxml_use_internal_errors(true);

        $body = mb_convert_encoding($body, 'UTF-8', 'Windows-1251');

        $dom->loadHTML(sprintf('<html><head><meta http-equiv="content-type" content="text/html; charset=utf-8"></head>%s</html>', $body));

        $this->xpath = new \DOMXPath($dom);
    }
}