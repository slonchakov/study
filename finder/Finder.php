<?php

namespace Finder;

class Finder
{
    const SLEEP_BETWEEN_QUERIES = 120;
    const SLEEP_BETWEEN_RESULT_PAGES = 5;

    private $url = 'https://www.google.com.ua/search?cad=h&q=';
    private $step = 10;
    /**
     * @var array
     */
    private $queries;

    public function process(): void
    {
        $response = "\n";

        foreach ($this->queries as $index => $query) {

            $start = 0;
            $limit = 100;
            $stop = false;

            echo "\n" . 'Loading: '. $query ."\n";

            while($start < $limit && !$stop) {

                sleep(self::SLEEP_BETWEEN_RESULT_PAGES);

                echo '-';

                $startQuery = '';

                if ($start > 0) {
                    $startQuery = '&start='. $start;
                }

                $request = $this->url . urlencode($query) . $startQuery;

                $content = @file_get_contents($request);

                if (!$content) {
                    var_dump($http_response_header); die;
                }

                $body = substr($content, strpos($content, '<body'), -7);

                $domParser = new DomParser();
                $links = $domParser->getLinks($body);

                if ($links->count()) {
                    $response .=  'Links count is:' . $links->count() ."\n";
                    $page = ($start / $this->step) + 1;
                    $response .=  'Page: '. $page ."\n";

                    $response .= $query . ' has been found on page '. $page ."\n";
                    $stop = true;

                    foreach ($links as $link) {
                        parse_str($link->getAttribute('href'), $l);
                        $response .=  $l['/url?q'] ."\n";
                        $response .=  $link->nodeValue ."\n";
                    }
                }

                $start += $this->step;
            }

            if ($this->hasNext($index)) {
                sleep(self::SLEEP_BETWEEN_QUERIES);
            }
        }

        echo $response;
    }

    private function hasNext($index)
    {
        return !empty($this->queries[$index + 1]);
    }

    public function setQueries(array $queries)
    {
        $this->queries = $queries;
    }

}