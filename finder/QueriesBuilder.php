<?php

namespace Finder;

class QueriesBuilder
{

    private $dir = __DIR__ . '/config/';
    private $limit = 30;

    public function build(): array
    {

        return ['Кеды подростковые'];

        $queries = [];

        if (!is_dir($this->dir)) {
            throw new \Exception('There is no this directory');
        }

        $files = scandir($this->dir);

        foreach ($files as $file) {
            if ($file == '.' || $file == '..') {
                continue;
            }

            if (count($queries) >= $this->limit) {
                break;
            }

            $part = include($this->dir . $file);
            $queries = array_merge($queries, $part);
        }

        return $queries;
    }
}