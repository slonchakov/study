<?php

/*
 *  интерпритатор
 * Шаблон для создания подязыка, внутри языка. Передавая строки в программу переводить их в команды
 * Состоит из контектса, терминальных выражений(простое выражение) и нетерминальных выражений(комплексное выражение)
 */

interface Expression
{
    public function interpret(array $context): int;
}

class TerminalExpression implements Expression
{

    /**
     * @var string
     */
    private $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function interpret(array $context): int
    {
        return intval($context[$this->name]);
    }
}

abstract class NonTerminalExpression implements Expression
{
    protected $left;
    protected $right;

    public function __construct(Expression $left, ?Expression $right)
    {
        $this->left = $left;
        $this->right = $right;
    }

    abstract public function interpret(array $context): int;
}

class PlusExpression extends NonTerminalExpression
{

    public function interpret(array $context): int
    {
        return intval($this->left->interpret($context) + $this->right->interpret($context));
    }
}

class MinusExpression extends NonTerminalExpression
{

    public function interpret(array $context): int
    {
        return intval($this->left->interpret($context) - $this->right->interpret($context));
    }
}


class InterpreterClient
{
    protected function parseList(array &$stack, array $list, int &$index)
    {
        $token = $list[$index];

        switch($token) {
            case '+':
                [$left, $right] = $this->fetchArguments($stack, $list, $index);

                return new PlusExpression($left, $right);

            case '-':
                [$left, $right] = $this->fetchArguments($stack, $list, $index);

                return new MinusExpression($left, $right);
            default:
                return new TerminalExpression($token);
        }
    }

    protected function fetchArguments(array &$stack, array $list, int &$index): array
    {
        $left = array_pop($stack);
        $right = array_pop($stack);

        if ($right === null) {
            ++$index;
            $this->parseListAndPush($stack, $list, $index);
            $right = array_pop($stack);
        }

        return [$left, $right];
    }

    protected function parseListAndPush(array &$stack, array $list, int &$index)
    {
        array_push($stack, $this->parseList($stack, $list, $index));
    }

    protected function parse(string $data): Expression
    {
        $stack = [];
        $list = explode(' ', $data);
        for  ($index = 0; $index < count($list); $index++) {
            $this->parseListAndPush($stack, $list, $index);
        }

        return array_pop($stack);
    }

    public function main()
    {
        $data = 'u + v - w + z';
        $expr = $this->parse($data);
        $context = ['u' => 3, 'v' => 7, 'w' => 35, 'z' => 9];
        $res = $expr->interpret($context);
        echo "result: $res". PHP_EOL;
    }
}

$client = new InterpreterClient();
$client->main();