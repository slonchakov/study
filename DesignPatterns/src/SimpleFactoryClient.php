<?php

namespace DesignPatterns;


class SimpleFactoryClient {

	private $factory;

	public function __construct(ISimpleFactory $factory) {
		$this->factory = $factory;
	}

	public function getItem()
	{
		return $this->factory->create();
	}

}