<?php



namespace DesignPatterns;

require __DIR__ . '/../../vendor/autoload.php';

class StaticFactory
{
	public static function create($type): Item
	{
		if ($type == 1) {
			return new FirstItem();
		}

		return new SecondItem();
	}
}