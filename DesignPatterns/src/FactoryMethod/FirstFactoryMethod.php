<?php


namespace DesignPatterns\FactoryMethod;


use DesignPatterns\FirstItem;
use DesignPatterns\Item;

class FirstFactoryMethod extends FactoryMethod
{
    public function createItem(): Item
    {
        return new FirstItem();
    }
}