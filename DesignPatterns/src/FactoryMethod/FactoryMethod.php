<?php


namespace DesignPatterns\FactoryMethod;


use DesignPatterns\Item;

abstract class FactoryMethod
{
    public function getItemName() {
        $item = $this->createItem();
        return get_class($item);
    }

    abstract public function createItem(): Item;
}