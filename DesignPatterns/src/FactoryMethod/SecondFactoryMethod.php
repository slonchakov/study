<?php


namespace DesignPatterns\FactoryMethod;

use DesignPatterns\Item;
use DesignPatterns\SecondItem;

class SecondFactoryMethod extends FactoryMethod
{
    public function createItem(): Item
    {
        return new SecondItem();
    }
}