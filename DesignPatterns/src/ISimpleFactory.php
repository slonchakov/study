<?php

namespace DesignPatterns;

interface ISimpleFactory
{
	public function create();

}