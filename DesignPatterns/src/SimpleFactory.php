<?php

namespace DesignPatterns;


class SimpleFactory implements ISimpleFactory
{

	public function create(): SimpleItem
	{
		return new SimpleItem();
	}
}