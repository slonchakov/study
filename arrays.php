<?php

/*
 * Book modern PHP best practices
 */
//$str = 'EXCELENT';
//
//echo implode(array_filter(str_split($str), function($item){
//    return $item === 'E';
//}));

// returns EEE

/*
$str = "<a href='https://google.com'>Hi</a>";

//$str = htmlentities($str);
$str = htmlspecialchars($str);

echo $str;
*/

/*
 * Interface
 * Only public methods
 * Only methods
 * I can define constant and use it
 */

interface Foo {
    const HI = 1;
    public function method();
}

//echo Foo::HI;

class FooImplements implements Foo {

    public function method()
    {
        return 'from foo';
    }

    public function __toString()
    {
        return 'FooImplements';
    }
}

/*
 * Abstract class
 *
 * Abstract methods couldn't be private
 * I can add private method but it can't be abstract
 * I can change number of parameters, but they must be optional
 * I can't change type of parameter in child class
 * I can change type of parameters if not use them
 */

// abstract class Bar {
//
//    private function privateMethod($ar){
//        print_r($ar);
//    }
//}
//
//class C extends Bar {
//
//    public function privateMethod($foo)
//    {
//        return $foo;
//    }
//}
//
//$c = new C;
//
//echo $c->privateMethod('123');
//echo $c->privateMethod(new FooImplements());

/*
 * Session
 *
 * Array $_SESSION doesn't destroy after session destroy
 */

/*
 * session_start();

$_SESSION[1] = 1;
$_SESSION[2] = 2;

session_destroy();

var_dump($_SESSION);
*/

/*
 * ПОдавление ошибок с помощью оператора @ или поднятие уровня вывода ошибок что php пропускал Warning
 * Ignore error messages with control operator @ or rise level of error reporting to skip Warnings
 */
//error_reporting(E_ERROR);
//
//$my_file = file ('non_existent_file') or
//die ("Ошибка при открытии файла: сообщение об ошибке было таким: '$php_errormsg'");

/*
 * Link can change original
 * if original variable deleted value can be still accessible
 * If unset $a, $b still returns 1
 * $b not link to $a, it is a link to a memory where store value 1
 * $b not depends on $a
*/
/*
 * $a = 1;
$b = &$a;
echo $b;
unset($a);
echo $b;
echo $a;
*/
/*
class A {
    public $a = 1;
}

$obj = new A;

function increase($obj) {
    $obj->a += 1;
}

increase($obj);
increase($obj);
increase($obj);

echo $obj->a;
*/

/*
 * ++ and --
 */
/*
 * $a = 1;
echo $a++; // first echo then ++
echo $a;
*/

/*
 * To get char in current position I have to use mb_substr
 */

$str = 'строка';
// mb_substr ( string $str , int $start [, int $length = NULL [, string $encoding = mb_internal_encoding() ]] ) : string
// echo mb_substr($str, 0, 1); // first char in a string
//echo mb_internal_encoding();

/*
 * str_split() - break string to array mb_str_split() for multibyte strings
 * preg_split('//u', $str, null, PREG_SPLIT_NO_EMPTY)
 */
//$str = 'Привет';
//print_r(preg_split('//u', $str, null, PREG_SPLIT_NO_EMPTY));

/*
 * print_r(str_split($str));
print_r(mb_str_split($str)); // PHP 7.4
$res = [];

for ($i = 0; $i < mb_strlen($str); $i++) {
    $res[] = mb_substr($str, $i,1);
}
print_r($res);

print_r(preg_split('//u', $str, -1, PREG_SPLIT_NO_EMPTY));
print_r(explode('', $str)); // explode can't be used because delimiter can't be empty string
*/

/*
 * strrev - reverse string
 */

//$a = [1,-15,108,22,57];
//rsort($a);
//var_dump($a);
//
//$a = [
//    [
//        'name' => 1,
//        'val' => 'str 111',
//    ],
//    [
//        'name' => -15,
//        'val' => 'str 2'
//    ],
//    [
//        'val' => 'str 13 ',
//        'name' => 22,
//    ],
//    [
//        'name' => 108,
//        'val' => 'str 4'
//    ]
//];
////sort($a);
//$keys = array_column($a, 'name');
//asort($keys);
//
//var_dump($keys);
//
//array_multisort(array_keys($keys), $a);
//
//var_dump($a);
//

$a = ['key' => ['a' => 1,10], 'key2' => 2];
$b = ['key' => ['a' => 1,8,9], 'key2' => 2, 'key4' => 5];
// array_merge values from first overrides by next arrays
// array_diff returns all values from first array which not found in other arrays, keys and values comparing
// array_diff_key - ignore values
var_dump(array_merge_recursive($a, $b)); // result [key => [ a => [1,1], 10,8,9]]
//$a = [1,-15,108,22,57];
//rsort($a);
//var_dump($a);
//
//$a = [
//    [
//        'name' => 1,
//        'val' => 'str 111',
//    ],
//    [
//        'name' => -15,
//        'val' => 'str 2'
//    ],
//    [
//        'val' => 'str 13 ',
//        'name' => 22,
//    ],
//    [
//        'name' => 108,
//        'val' => 'str 4'
//    ]
//];
////sort($a);
//$keys = array_column($a, 'name');
//asort($keys);
//
//var_dump($keys);
//
//array_multisort(array_keys($keys), $a);
//
//var_dump($a);
//

$a = ['key' => ['a' => 1,10], 'key2' => 2];
$b = ['key' => ['a' => 1,8,9], 'key2' => 2, 'key4' => 5];
// array_merge values from first overrides by next arrays
// array_diff returns all values from first array which not found in other arrays, keys and values comparing
// array_diff_key - ignore values
var_dump(array_merge_recursive($a, $b)); // result [key => [ a => [1,1], 10,8,9]]