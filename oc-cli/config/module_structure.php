<?php

return [
    'admin/controller/extension/module' => [
        'callback' => 'controllerTemplate',
        'template' => 'admin-controller',
        'classPrefix' => 'ControllerExtensionModule',
    ],
    'admin/model/extension/module' => [
        'callback' => 'modelTemplate',
        'template' => 'model',
        'classPrefix' => 'ModelExtensionModule',
    ],
    'admin/language/en-gb/extension/module' => [
        'callback' => 'simpleTemplate',
        'template' => 'empty-php',
    ],
    'admin/language/ru-ru/extension/module' => [
        'callback' => 'simpleTemplate',
        'template' => 'empty-php',
    ],
    'admin/theme/default/view' => [
        'callback' => 'simpleTemplate',
        'template' => 'admin-view',
    ],
    'catalog/controller/extension/module' => [
        'callback' => 'controllerTemplate',
        'template' => 'controller',
        'classPrefix' => 'ControllerExtensionModule',
    ],
    'catalog/model/extension/module' => [
        'callback' => 'modelTemplate',
        'template' => 'model',
        'classPrefix' => 'ModelExtensionModule',
    ],
    'catalog/language/en-gb/extension/module' => [
        'callback' => 'simpleTemplate',
        'template' => 'empty-php',
    ],
    'catalog/language/ru-ru/extension/module' => [
        'callback' => 'simpleTemplate',
        'template' => 'empty-php',
    ],
    'catalog/view/theme/default/extension/module' => [
        'fileType' => '.tpl',
        'template' => '',
    ],
];
