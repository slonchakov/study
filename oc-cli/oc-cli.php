<?php

/**
 * Command schema
 * php oc-cli command:name arg1=value1 arg2
 */

use Occli\Console\Config;
use Occli\Console\Kernel;

require_once __DIR__ . '/vendor/autoload.php';

$config = Config::getInstance();

$config->set('rootDir', __DIR__);

$ocCli = new Kernel($argv);

foreach ($config->get('commands') as $commandName => $commandExecutable) {
    $ocCli->addCommand($commandName, new $commandExecutable());
}

$ocCli->execute();