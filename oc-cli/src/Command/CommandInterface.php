<?php


namespace Occli\Command;


use Occli\Console\ArgumentCollection;

interface CommandInterface
{
    public function execute(ArgumentCollection $arguments);
}