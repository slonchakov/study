<?php

namespace Occli\Command\Module;

use Occli\Command\CommandInterface;
use Occli\Console\ArgumentCollection;
use Occli\Console\Config;

class CreateModule implements CommandInterface
{
    private $templateDir = __DIR__ . '/fileTemplates';

    /**
     * Configurable items
     */
    private $dir = 'demo';
    private $vendor = 'centum';
    private $moduleName = 'demo';

    /**
     * Default values
     */
    private $suffix = '.php';
    private $template = 'empty-php';

    /**
     * @var Config
     */
    private $config;

    public function __construct()
    {
        $this->config = Config::getInstance();
    }

    public function execute(ArgumentCollection $arguments)
    {
        $this->configure($arguments);

        foreach ($this->config->get('module_structure') as $dir => $options) {

            $fileDirectory = $this->createPath($dir);
            $fileName = $this->moduleName . ($options['fileType'] ?? $this->suffix);
            $template = $options['template'] ?? $this->template;

            try {
                if (!empty($options['callback'])) {
                    file_put_contents(
                        $fileDirectory . '/' . $fileName,
                        call_user_func([$this, $options['callback']], $template, $this->moduleName)
                    );
                } else {
                    file_put_contents($fileDirectory . '/' . $fileName, $template);
                }

                echo $fileDirectory . '/' . $fileName . ' has been added' . "\n";
            } catch (\Exception $e) {
                echo $e->getMessage();
                exit;
            }
        }

        echo "\nModule created completely\n";
    }

    private function configure(ArgumentCollection $arguments)
    {

        try {
            if ($arguments->has('dir')) {
                $this->dir = $arguments->get('dir');
            }

            if ($arguments->has('vendor')) {
                $this->vendor = $arguments->get('vendor');
            }

            if ($arguments->has('name')) {
                $this->moduleName = $arguments->get('name');
            } else {
                throw new \Exception('Module name is required, add module=name argument');
            }

        } catch (\Exception $e) {
            echo $e->getMessage();
            exit;
        }
    }

    private function createPath(string $dir)
    {
        $fileDirectory = implode('/', [$this->config->get('rootDir'), $this->dir, $dir, $this->vendor]);

        if (!is_dir($fileDirectory)) {
            try {
                mkdir($fileDirectory, 0755, true);
            } catch (\Exception $e) {
                echo $e->getMessage();
            }
        }

        return $fileDirectory;
    }

    private function simpleTemplate($template): string
    {
        return $this->getTemplateContent($template);
    }

    private function controllerTemplate($template, $name): string
    {
        $className = $this->buildClassName('ControllerExtensionModule', $name);
        return $this->getTemplateContent($template, [$className, $name]);
    }

    private function modelTemplate($template, $name): string
    {
        $className = $this->buildClassName('ModelExtensionModule', $name);
        return $this->getTemplateContent($template, [$className]);
    }

    private function buildClassName($prefix, $name): string
    {
        $parts = explode('_', $name);
        $result = $prefix . ucfirst($this->vendor);
        foreach ($parts as $part) {
            $result .= ucfirst(trim($part));
        }

        return $result;
    }

    private function getTemplateContent($name, $args = []): string
    {
        $result = file_get_contents($this->templateDir . '/' . $name);

        if (!empty($args)) {
            return vsprintf($result, $args);
        }

        return $result;
    }
}
