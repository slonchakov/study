<?php


namespace Occli\Console;


class ArgumentCollection
{
    private $arguments;

    public function push(Argument $argument)
    {
        $this->arguments[$argument->getName()] = $argument;
    }

    public function get(string $name)
    {
        return $this->arguments[$name]->getValue() ?? null;
    }

    public function has(string $name): bool
    {
        return $this->arguments && key_exists($name, $this->arguments);
    }
}
