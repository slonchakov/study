<?php


namespace Occli\Console;


/**
 * Class Config - Singleton
 * @package Occli\Console
 */
class Config
{
    private $config;
    public static $instance = null;

    private function __construct()
    {
        $configDir = __DIR__ . '/../../config';

        if ($handle = opendir($configDir)) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != "..") {
                    $configName = basename($entry, '.php');
                    $this->config[$configName] = include($configDir . '/' . $entry);
                }
            }
            closedir($handle);
        }
        self::$instance = $this;
    }

    public static function getInstance(): self
    {
        if (!self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    public function get($key)
    {
        return $this->config[$key] ?? null;
    }

    public function set($key, $value)
    {
        $this->config[$key] = $value;
    }

    private function __clone(){}

    private function __wakeup(){}

}
