<?php


namespace Occli\Console;


class Argument
{

    private $name;
    private $value = '';

    public function __construct(string $argument)
    {
        if (strpos($argument, '=') !== false) {
            [$name, $value] = $this->parseOption($argument);

            $this->name = $name;
            $this->value = $value;
        } else {
            $this->name = $argument;
        }
    }

    public function getName()
    {
        return $this->name;
    }

    public function getValue()
    {
        return $this->value;
    }

    private function parseOption(string $argument): array
    {
        $explode = explode('=', $argument);
        return [trim($explode[0]), trim($explode[1])];
    }
}
