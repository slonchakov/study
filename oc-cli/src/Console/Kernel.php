<?php

namespace Occli\Console;

use Occli\Command\CommandInterface;

class Kernel
{
    const COMMAND_POSITION = 1;
    const ARGUMENTS_START_POSITION = 2;
    private $command;
    private $arguments = [];
    /**
     * @var CommandInterface
     */
    private $commands;

    public function __construct(array $arguments)
    {
        $this->command = $arguments[static::COMMAND_POSITION] ?? null;

        $this->arguments = new ArgumentCollection();

        for ($i= static::ARGUMENTS_START_POSITION; $i < count($arguments); $i++) {

            $this->arguments->push(new Argument($arguments[$i]));
        }
    }

    public function addCommand(string $name, CommandInterface $command)
    {
        $this->commands[$name] = $command;
    }

    public function execute()
    {
        if (!empty($this->commands[$this->command])) {
            $this->commands[$this->command]->execute($this->arguments);
        } else {
            echo "command $this->command not found \n ";
        }

    }

}
