<?php

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

include 'vendor/autoload.php';

/*
 * Laravel
 */
/*
 * config/graphql.php

 */
[
	'schemas' => [
		'default' => [
			'query' => [
				'categories' => CategoriesQuery::class
			],
			'mutation' => [
				'create_category' => CategoryCreateMutation::class
			],
			'middleware' => [],
			'method' => ['get', 'post']
		]
	],
	'types' => [
		'Category' => CategoryType::class
	]
];


class CategoryType extends GraphQLType
{
	protected $attributes = [
		'name' => 'Category'
	];

	public function fields() : array
	{
		// маппер  полей таблицы к сущности типа
		return [
			'id' => [
				'type' => Type::nonNull(Type::int())
			],
			'name' => [
				'type' => Type::nonNull(Type::string()),
				'description' => 'The name of category',
				'resolve' => function($root, $args) {
					return stripslashes($root->description);
				}
			],
			'events' => [
				'type' => Type::listOf(GraphQL::type('Event')),
				'privacy' => function($root, $args) {
					// метод в котором можно определить показывать это поле в конкретном случае
					return true;
				}
			],
			'query_at' => [
				'type' => Type::string() // этого поля нет в базе для него пишется резолвер и это поле добавляется к каждому запросу
			]
		];
	}

	protected function resolveQueryAtField($root, $arg)
	{
		return now()->toDateTimeString();
	}
}

class Category
{
	// Model

	public static function all() {
		return [];
	}

	public static function find( $id ) {
		return 1;
	}

	public static function create( $args ) {
		return true;
	}

	public function events()
	{
		// return $this->hasMany('events') - eloquent method
	}
}

// Описываем запросы
class CategoriesQuery extends \Rebing\GraphQL\Support\Query
{

	public function type(): Type {
		// TODO: Implement type() method.
		return Type::listOf(GraphQL::type('Category'));
	}

	public function resolve()
	{
		return Category::all();
	}
}

class CategoryQuery extends \Rebing\GraphQL\Support\Query
{

	public function type(): Type {
		// TODO: Implement type() method.
		return GraphQL::type('Category');
	}

	public function args(): array
	{
		return [
			'id' => [
				'name' => 'id',
				'type' => Type::int(),
				'rules' => ['required']
			]
		];
	}

	public function resolve($root, $args)
	{
		return Category::find($args['id']);
	}
}

class CategoryCreateMutation extends \Rebing\GraphQL\Support\Mutation
{

	protected $attributes = [
		'name' => 'create_category'
	];

	public function type(): Type {
		return GraphQL::type('Category');
	}

	public function args():array
	{
		return [
			'name' => [
				'name' => 'name',
				'type' => Type::string()
				]
		];
	}

	protected function rules(array $args = []): array
	{
		// валидация на базе laravel
		return [
			'name' => ['required', 'string', 'max:64']
		];
	}

	public function resolve($root, $args)
	{
		return Category::create($args);
	}
}