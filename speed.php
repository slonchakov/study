<?php

//class Foo
//{
//    public function method()
//    {
//        return 1;
//    }
//}
//
//abstract class Foo2
//{
//    public function method()
//    {
//        return $this->callFoo2();
//    }
//
//    abstract protected function callFoo2();
//}
//
//class Foo3 extends Foo2
//{
//    protected function callFoo2()
//    {
//        return 1;
//    }
//}

//$foo = new Foo();
// 0.000016927719116211

//$foo->method();
// 0.000041007995605469

//$a = 4;
//$b = 2;
//
//$foo3 = new Foo3();
//$foo3->method();
// Polimorfic call
// 0.000051021575927734


//$start = microtime(true);
//$foo3->method();
//echo microtime(true) - $start;

// Empty lines between microtime calls
//0.0000050067901611328
// 5.0067901611328E-6
//$a + $b;
// 0.0000061988830566406
//$a - $b;
// 0.0000059604644775391
//$a * $b;
// 0.0000050067901611328 - multiply as fast as empty call
//$a / $b;
// 0.000010013580322266 - divide is twice slower then other operations


echo xdebug_get_profiler_filename();