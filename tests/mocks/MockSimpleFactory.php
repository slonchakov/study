<?php

namespace Mocks;

use DesignPatterns\ISimpleFactory;
use DesignPatterns\Item;

class MockSimpleFactory implements ISimpleFactory
{
	public function create(): Item
	{
		return new MockSimpleItem();
	}
}