<?php


use DesignPatterns\SimpleFactory;
use DesignPatterns\SimpleFactoryClient;
use Mocks\MockSimpleFactory;
use PHPUnit\Framework\TestCase;

class SimpleFactoryTest extends TestCase
{

	public function testSimpleFactory()
	{
		$client = new SimpleFactoryClient(new SimpleFactory());
		$item = $client->getItem();

		$mockClient = new SimpleFactoryClient(new MockSimpleFactory());
		$mockItem = $mockClient->getItem();

		$this->assertEquals(class_implements($mockItem), class_implements($item));
	}
}
