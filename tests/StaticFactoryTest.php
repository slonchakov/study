<?php

require __DIR__ .'/../vendor/autoload.php';


use DesignPatterns\StaticFactory;
use PHPUnit\Framework\TestCase;

class StaticFactoryTest extends TestCase
{

	public function testStaticFactory()
	{

		$item = StaticFactory::create(1);
		$this->assertContains(DesignPatterns\Item::class, class_implements($item));
	}

	public function testBadStaticFactory()
	{

		$item = StaticFactory::create(1);
		$this->assertNotContains('NotItem', class_implements($item));
	}

}
